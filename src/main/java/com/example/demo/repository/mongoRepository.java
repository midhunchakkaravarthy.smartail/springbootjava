package com.example.demo.repository;

import com.example.demo.model.mongoModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface mongoRepository extends MongoRepository<mongoModel,String> {
}
