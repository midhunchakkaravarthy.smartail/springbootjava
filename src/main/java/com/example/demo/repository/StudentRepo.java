package com.example.demo.repository;// Java Program to Illustrate BookRepo File

import com.example.demo.model.Student;
//import com.globallogic.spring.mongodb.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface StudentRepo extends MongoRepository<Student, String> {
    @Query("{'rid':?0}")
    Student findByrid(int rid);
}