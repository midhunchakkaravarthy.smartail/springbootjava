package com.example.demo.repository;

import com.example.demo.model.Student;
import com.example.demo.model.studentModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface StudentModelRepo extends MongoRepository<studentModel, Integer> {
    @Query(value="{'roll_no':?0}",delete = true)
    studentModel deleteByRno(int roll_no);
    @Query("{'roll_no':?0}")
    studentModel findByRno(int roll_no);
}