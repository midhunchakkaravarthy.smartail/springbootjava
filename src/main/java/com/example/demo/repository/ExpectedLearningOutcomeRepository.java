package com.example.demo.repository;

import com.example.demo.model.ExpectedLearningOutcome;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpectedLearningOutcomeRepository extends MongoRepository<ExpectedLearningOutcome,String> {
    @Query("{$and:[{'standard':?0},{'subject':?1},{'chapters.number':?2}]}")
    ExpectedLearningOutcome findByStandardandSubjectandChapter(String standard, String subject,String chapter);
}