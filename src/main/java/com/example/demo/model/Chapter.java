package com.example.demo.model;

import lombok.Data;

@Data
public class Chapter {
    public String number;
    public String name;
}
