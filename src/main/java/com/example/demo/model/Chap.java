package com.example.demo.model;

import lombok.Data;

import java.util.List;

@Data
public class Chap {
    public String number;
    public List<String> elo;
}
