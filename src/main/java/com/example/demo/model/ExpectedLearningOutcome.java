package com.example.demo.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Data
@Document(collection = "outcome_chapter_mapping")
public class ExpectedLearningOutcome {
    public String standard;
    public String subject;
    public Chapter chapters;
    public List<LearningOutcome> learningOutcomes;
}