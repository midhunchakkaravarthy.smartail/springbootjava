package com.example.demo.model;

import lombok.Data;

import java.util.List;

@Data
public class Subject {
    public String subjectname;
    public List<Chap> chapters;
}
