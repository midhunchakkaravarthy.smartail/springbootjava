package com.example.demo.model;

import lombok.Data;

import java.util.List;

@Data
public class ELO {
    public String standard;
    public List<Subject> subject;
}
