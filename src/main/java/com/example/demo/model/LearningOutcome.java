package com.example.demo.model;

import lombok.Data;

@Data
public class LearningOutcome {
    public String number;
    public String name;
}