package com.example.demo.controller;

import com.example.demo.model.Chapter;
import com.example.demo.model.ELO;
import com.example.demo.model.ExpectedLearningOutcome;
import com.example.demo.model.LearningOutcome;
import com.example.demo.repository.ExpectedLearningOutcomeRepository;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class AddEloMappingController {

    @Autowired
    public ExpectedLearningOutcomeRepository repository;

    @PutMapping("/utility")
    public void list(){
        ExpectedLearningOutcome outcome=new ExpectedLearningOutcome();
        Chapter chapter=new Chapter();
       // List<LearningOutcome> chapter_elo_list_1=new ArrayList<LearningOutcome>();
        String[][] words = new String[1][1];
        String[] chapter_id = new String[1];
        String[][] sentence = new String[1][1];
        String[] learningOutcome = new String[1];
        String[] elo_id = new String[1];
        try{
            ObjectMapper mapper=new ObjectMapper();
            InputStream inputStream=new FileInputStream(new File("src/main/resources/json/user.json"));
            TypeReference<List<ELO>> typeReference=new TypeReference<List<ELO>>() {
            };
            List<ELO> elo=mapper.readValue(inputStream,typeReference);
            mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
            for(ELO e:elo){
                e.subject.stream().forEach(subject -> {
                    outcome.standard=e.standard;
                    outcome.subject = subject.subjectname;
                    //System.out.println(outcome.standard+" ---> "+outcome.subject);
                    subject.chapters.stream().forEach(chap -> {
                          List<LearningOutcome> chapter_elo_list=new ArrayList<LearningOutcome>();
                          //System.out.println(chap);
                          chapter.setNumber(chap.number);
                          words[0] =chapter.number.toLowerCase().split(" ");
                          chapter_id[0] =String.join("_", words[0]);
                          outcome.setChapters(chapter);
                          chap.elo.stream().forEach(eloutcome->{
                              LearningOutcome learningOutcome1=new LearningOutcome();
                              sentence[0] =eloutcome.toLowerCase().split(" ");
                              learningOutcome[0] =String.join("_", sentence[0]);
                              elo_id[0] =outcome.standard + "-" + subject.subjectname.charAt(0) + subject.subjectname.charAt(1) + subject.subjectname.charAt(2) + "-" + chapter_id[0] + "-" + learningOutcome[0];
                              learningOutcome1.setName(eloutcome);
                              learningOutcome1.setNumber(elo_id[0]);
                              chapter_elo_list.add(learningOutcome1);;
                              outcome.setLearningOutcomes(chapter_elo_list);
                          });
                          repository.save(outcome);
                    });
                });
            }
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(JsonParseException e){
            e.printStackTrace();
        }catch(JsonMappingException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    @GetMapping("/utility")
    public ExpectedLearningOutcome fetchElo(@RequestParam(required = false) String standard,@RequestParam(required = false) String subject,@RequestParam(required = false) String chapters) {
        return repository.findByStandardandSubjectandChapter(standard,subject,chapters);
    }
}