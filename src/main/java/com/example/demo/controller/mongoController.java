package com.example.demo.controller;

import com.example.demo.model.mongoModel;
import com.example.demo.repository.mongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class mongoController {

    @Autowired
    public mongoRepository repo;

    @PutMapping("/mongo")
    public String addStudent(@RequestBody mongoModel data){
        repo.save(data);
        return "Successfully added";
    }

    @GetMapping("/mongo")
    public List<mongoModel> getStudent(){
        return repo.findAll();
    }
}
