package com.example.demo.controller;

import com.example.demo.model.StudentDetails;
import com.example.demo.model.studentModel;
import com.example.demo.repository.StudentDetailsRepo;
import com.example.demo.repository.StudentModelRepo;
import com.example.demo.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class StudentDetailsController {

    @Autowired
    private StudentDetailsRepo repo;

    @PostMapping("/check")
    public boolean login(@RequestBody StudentDetails student){
        if(student.name.equals("Midhuli") && student.password.equals("jeevi")){
            return true;
        }
        else{
            return false;
        }
    }
}
