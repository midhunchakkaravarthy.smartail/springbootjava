package com.example.demo.controller;

import com.example.demo.model.Student;
import com.example.demo.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// Annotation
@RestController
@CrossOrigin(origins = "http://localhost:4200")
// Class
public class StudentController {

    @Autowired
    private StudentRepo repo;

    @RequestMapping(value = "/addStudent", method = RequestMethod.POST)
    public String saveBook(@RequestBody Student student) {
        System.out.println(student);
        repo.save(student);
//        if(student.bookName.equals("ps") && student.authorName.equals("kalki")){
//            return "true";
//        }
//        else{
//            return "false";
//        }

        return "Added Successfully";
    }



    @PutMapping("/addStudent2")
    public String saveBook2(@RequestBody Student student) {
        repo.save(student);

        return "Added Successfully";
    }



    @GetMapping("/findAllStudents")
    public List<Student> getBooks() {

        return repo.findAll();
    }

    @DeleteMapping("/delete")
    public String deleteBook(@RequestParam("rid") String rid) {
        repo.deleteById(rid);
        return "Deleted Successfully";
    }

    @GetMapping("/find")
    public Student findValue(@RequestParam(value = "rid") int rid) {

        return repo.findByrid(rid);
    }

//    @PutMapping("/updat")
}
