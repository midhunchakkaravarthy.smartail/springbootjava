package com.example.demo.controller;

import com.example.demo.model.studentModel;
import com.example.demo.repository.StudentModelRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class StudentModelController {
    @Autowired
    private StudentModelRepo repo;

    @CrossOrigin
    @PutMapping("/student")
    public String addStudent(@RequestBody studentModel s){
        repo.save(s);
        return "Successfully added";
    }

    @CrossOrigin
    @PostMapping("/student")
    public String addStudents(@RequestBody studentModel s){
        repo.save(s);
        return "Successfully added";
    }

    @CrossOrigin
    @GetMapping("/student")
    public List<studentModel> getStudent(){
        return repo.findAll();
    }

    @CrossOrigin
    @DeleteMapping ("/student/{rno}")
    public String deleteStudent(@PathVariable int rno){
        repo.deleteByRno(rno);
        return "Deleted Successfully";
    }

    @CrossOrigin
    @PutMapping("/Student")
    public String updateStudent(@RequestBody studentModel s){
        Optional<studentModel> stud;
        stud= Optional.ofNullable(repo.findByRno(s.roll_no));
        if(stud.isPresent()){
            studentModel a=stud.get();
            a.name=s.name;
            repo.save(a);
            return "Updated";
        }
        else {
            repo.save(s);
            return "Added";
        }
//        return "Updated Successsfully";
    }

    @CrossOrigin
    @GetMapping("/studentid")
    public studentModel getStudentById(@RequestParam("rno") int rno){
        return repo.findByRno(rno);
    }
}
